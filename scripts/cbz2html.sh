#!/bin/bash
###################################################################### 
#Copyright (C) 2023  Kris Occhipinti
#https://filmsbykris.com

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation version 3 of the License.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
###################################################################### 

function error(){
  red=`echo -en "\e[31m"`
  normal=`echo -en "\e[0m"`
  echo -e "$red$@$normal" >&2
  exit 1
}

function help(){
  name="$(basename $0)"
  echo "
Useage: $name <comic file>
Example: $name comic.cbz
  "
  exit
}

[[ $1 ]] || help
[[ -f "$1" ]] || error "$1 does not exist"

file="$1"
comic="${file%.*}"
unzip "$file" ||error bad file

cd "$comic"|| error $comic does not exits.

echo "comic = '$comic';" > "comic.js"
echo "const pages = [" >> "comic.js"


for page in *.jpg
do
  echo "'$page',"
done >> "comic.js"

echo "]" >> "comic.js"

wget "https://gitlab.com/metalx1000/Comic-Converter/-/raw/master/layouts/index.html?ref_type=heads&inline=false" -O index.html

